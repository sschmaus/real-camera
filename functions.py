# Libraries
import bpy
import os


def read_filmic(path):
    nums = []
    with open(path) as filmic_file:
        for line in filmic_file:
            nums.append(float(line))
    return nums


# Globals
path = os.path.join(os.path.dirname(__file__), "looks/")
filmic_vhc = read_filmic(path + "Very High Contrast")
filmic_hc = read_filmic(path + "High Contrast")
filmic_mhc = read_filmic(path + "Medium High Contrast")
filmic_mc = read_filmic(path + "Medium Contrast")
filmic_mlc = read_filmic(path + "Medium Low Contrast")
filmic_lc = read_filmic(path + "Low Contrast")
filmic_vlc = read_filmic(path + "Very Low Contrast")


def contrast(log):
    if log < 1:
        look = bpy.context.scene.view_settings.look
        if look=="None":
            filmic = filmic_mc
        elif "Very High Contrast" in look:
            filmic = filmic_vhc
        elif "High Contrast" in look:
            filmic = filmic_hc
        elif "Medium High Contrast" in look:
            filmic = filmic_mhc
        elif "Medium Contrast" in look or "Base Contrast" in look:
            filmic = filmic_mc
        elif "Medium Low Contrast" in look:
            filmic = filmic_mlc
        elif "Low Contrast" in look:
            filmic = filmic_lc
        elif "Very Low Contrast" in look:
            filmic = filmic_vlc
        x = int(log * 4095)
        return filmic[x]
    else:
        return 1


def rgb_to_luminance(buf):
    lum = 0.2126 * buf[0] + 0.7152 * buf[1] + 0.0722 * buf[2]
    return lum



def register():
    return None

def unregister():
    return None
